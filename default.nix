{ nixpkgs ? import <nixpkgs> {}, use_32bit ? true }:

let
  _eff_pkgs = if use_32bit then nixpkgs.pkgsi686Linux else nixpkgs.pkgs;
  overlay = self: super: {
    stdenv = _eff_pkgs.overrideCC _eff_pkgs.stdenv _eff_pkgs.gcc48;
    pam = _eff_pkgs.pam.override { stdenv = _eff_pkgs.stdenv; };
    mesa-noglu = _eff_pkgs.mesa-noglu.override { stdenv = _eff_pkgs.stdenv; };
    rapidjson = _eff_pkgs.rapidjson.overrideAttrs (_: {
      NIX_CFLAGS_COMPILE = null;
      preConfigure = ''
        substituteInPlace CMakeLists.txt --replace "-Werror" ""
        substituteInPlace example/CMakeLists.txt --replace "-Werror" ""
      '';
    });
  };
  eff_pkgs = import <nixpkgs> { overlays = [ overlay (import /star/u/veprbl/my-nixpkgs-overlay) ]; };
  stdenv = eff_pkgs.stdenv;
  cons = stdenv.mkDerivation {
      name = "cons";
      src = builtins.filterSource
        (path: type: builtins.elem (baseNameOf path) [ "cons" "Construct" "ConsDefs.pm" "Conscript-standard" "RootCint.pl" ])
        /afs/rhic.bnl.gov/star/packages/SL17d/mgr;
      patchPhase = ''
        substituteInPlace ./cons --replace 'use lib ("./mgr",$ENV{STAR} ."/mgr");' 'use lib ("'$out'/lib/cons");'
        substituteInPlace ./Construct --replace 'use lib qw(./mgr $STAR/mgr);' 'use lib ("'$out'/lib/cons");'
      '';
      installPhase = ''
        install -Dm755 cons $out/bin/cons
        install -Dm755 RootCint.pl $out/bin/RootCint.pl
        install -Dm644 Construct $out/lib/cons/Construct
        install -Dm644 ConsDefs.pm $out/lib/cons/ConsDefs.pm
        install -Dm644 Conscript-standard $out/lib/cons/Conscript-standard
      '';
    };
  root = (eff_pkgs.root.override { inherit stdenv; gsl = eff_pkgs.gsl_1;
    libX11=null; libXpm=null; libXft=null; libXext=null; libGLU_combined=null;
      }).overrideAttrs (old: rec {
    version = "5.34.30"; # no significant difference to STAR's "5.34.30a_2"
    name = "root-${version}";
    src = nixpkgs.fetchurl {
      url = "https://root.cern.ch/download/root_v${version}.source.tar.gz";
      sha256 = "1iahmri0vi1a44qkp2p0myrjb6izal97kf27ljfvpq0vb6c6vhw4";
    };
    patches = [];
    cmakeFlags = old.cmakeFlags ++ [
      "-Dtable=ON"
      "-Dvc=ON"
      "-Dopengl=OFF"
      "-Dx11=OFF"
      ];
  });
in
  rec {
    inherit eff_pkgs stdenv root cons;
    inherit (eff_pkgs) curl libxml2 mysql;
    env =  stdenv.mkDerivation {
      name = "starenv";
      buildInputs = [
        cons
        root
        nixpkgs.cmake
        log4cxx
        mysql
        curl # for StStarLogger/StWsLogger.cxx
      ];
      STAR = "/afs/rhic.bnl.gov/star/packages/SL18a";
      STAR_SYS = "x8664_sl6";
      STAR_HOST_SYS = "sl64_gcc482";
      hardeningDisable = [ "stackprotector" "format" ];
    };
    # C++ packages are to be rebuilt with gcc48
    log4cxx = eff_pkgs.log4cxx.override { inherit stdenv; };
    fastjet = eff_pkgs.fastjet.override { inherit stdenv; };
    yoda = eff_pkgs.yoda.override { inherit stdenv; };
  }
